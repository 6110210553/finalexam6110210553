using PSUTutor.Models;
using Microsoft.EntityFrameworkCore;

namespace ProjectWeb.Data
{
    public class ProjectWebContext : DbContext
    {
       
        public DbSet<NewsPro> NewsList { get; set; }
    
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=ProjectWeb.db");
        }
    }
}